const usersUrl = fetch('https://ajax.test-danit.com/api/json/users');
const postsUrl = fetch('https://ajax.test-danit.com/api/json/posts');

class Card {
    constructor(name, email, title, body, id) {
        this.name = name;
        this.email = email;
        this.title = title;
        this.body = body;
        this.id = id;
    }
    createCard() {
        document.querySelector('.card').insertAdjacentHTML(
            'afterbegin',
            `<div class="card-container" id="${this.id}" >
            <div class="author">
            <h2 class="author-name">${this.name}</h2>
            <p class="author-nickname">${this.email}</p>
            </div>
            <p class="card-title"> ${this.title}</p>
            <p class="card-text"> ${this.body}</p>
            <button class="card-delete" onclick="deletePost(${this.id})">Delete post</button>
            </div>`
        )
    }
}

Promise.all([usersUrl, postsUrl])
    .then((data) =>
        Promise.all(data.map((data) => data.json()))
    )
    .then(([users, posts]) =>
        users.forEach((user) => {
            posts.forEach((post) => {
                if (user.id === post.userId) {
                    let postCard = new Card(
                        `${user.name}`,
                        `${user.email}`,
                        `${post.title}`,
                        `${post.body}`,
                        `${post.id}`
                    );
                    postCard.createCard();
                }
            })
        })
    );

function deletePost(id) {
    fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
        method: "DELETE",
    })
        .then(res => {
            if (res.ok) {
                document.querySelector(`[id='${id}']`).remove();
            }
        })
}


